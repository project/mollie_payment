<?php

/**
 * This file contains classes for the Mollie Payment module.
 */

/**
 * Mollie payment method controller.
 */
class MolliePaymentMethodController extends PaymentMethodController {

  public $payment_method_configuration_form_elements_callback = 'mollie_payment_method_configuration';

  public $payment_configuration_form_elements_callback = 'mollie_payment_configuration';

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->title = t('Mollie');
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    try {
      $method_data = $payment->method_data;
      $controller_data = $payment->method->controller_data;

      $client = mollie_payment_get_client($payment);
      if (!$client) {
        return;
      }

      $return_path = MOLLIE_PAYMENT_RETURN_PATH;
      $listener_path = MOLLIE_PAYMENT_LISTENER_PATH;
      $recurring_listener_path = MOLLIE_PAYMENT_RECURRING_LISTENER_PATH;
      if (!empty($controller_data['webhook_base_url'])) {
        $return_path = $controller_data['webhook_base_url'] . '/' . $return_path;
        $listener_path = $controller_data['webhook_base_url'] . '/' . $listener_path;
        $recurring_listener_path = $controller_data['webhook_base_url'] . '/' . $recurring_listener_path;
      }

      $payment_data = array(
        'amount' => array(
          'value' => number_format(floatval($payment->totalAmount(TRUE)), 2, '.', ''),
          'currency' => $payment->currency_code,
        ),
        'description' => $payment->description,
        'redirectUrl' => url($return_path . '/' . $payment->pid, array('absolute' => TRUE)),
        'webhookUrl' => url($listener_path . '/' . $payment->pid, array('absolute' => TRUE)),
      );

      // Do not include the webhook in test mode when no webhook base URL is
      // configured. But inform the use about the consequences.
      if ($controller_data['test_mode']
        && empty($controller_data['webhook_base_url'])) {
        unset($payment_data['webhookUrl']);
        drupal_set_message(
          t('No webhook URL was send to Mollie because you are in test mode without a webhook base URL configured. Mollie cannot report status updated on this payment.'),
          'warning'
        );
      }

      if (isset($method_data['mollie_payment_method'])) {
        $payment_data['method'] = $method_data['mollie_payment_method'];
      }

      if (isset($method_data['mollie_payment_issuer'])) {
        $payment_data['issuer'] = $method_data['mollie_payment_issuer'];
      }

      if (module_exists('payment_recurring')) {
        $recurring_info = payment_recurring_recurring_payment_info($payment);
        if (!empty($recurring_info)) {
          // This is a recurring payment.
          $payment_data['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_FIRST;

          /** @var \Mollie\Api\Resources\Customer $customer */
          // We need to created a customer first.
          $customer = $client->customers->create(array(
            'name' => $recurring_info['name'],
            'email' => $recurring_info['email'],
          ));

          /** @var \Mollie\Api\Resources\Payment $mollie_payment */
          $mollie_payment = $customer->createPayment($payment_data);
        }
      }

      if (!isset($mollie_payment)) {
        /** @var \Mollie\Api\Resources\Payment $mollie_payment */
        // Create the payment in the Mollie system.
        $mollie_payment = $client->payments->create($payment_data);
      }

      // Context data might not be the best location to store this information
      // because in fact it is not related to the context. We might consider
      // storing it in a separate database table just like the controller data
      // of the payment method. (The method_data in the payment object is
      // currently not stored.)
      $payment->context_data['payment'] = array(
        'id' => $mollie_payment->id,
      );
      entity_save('payment', $payment);

      $redirect_url = $mollie_payment->getCheckoutUrl();

      if (module_exists('payment_context')) {
        /** @var \Drupal\payment_context\PaymentContextInterface $context */
        $context = $payment->contextObj;
        $context->redirect($redirect_url);
      }
      else {
        drupal_goto($redirect_url, array(), 303);
      }
    }
    catch (Exception $e) {
      watchdog('mollie_payment', 'Payment execution failed: <pre>@data</pre>', array('@data' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }
}
