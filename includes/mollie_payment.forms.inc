<?php

/**
 * @file
 * Forms for Mollie Payment.
 */

/**
 * Form callback for mollie_payment_account_configuration_form.
 */
function mollie_payment_account_configuration_form(array $form, array &$form_state) {
  // Load the installer.
  mollie_payment_get_installer();

  // Mollie account creation.
  // TODO: Remove hardcoded language.
  $url = url(
    \Drupal\mollie_payment\MolliePaymentInstaller::MOLLIE_CREATE_ACCOUNT_URL,
    array('query' => array('lang' => 'en'))
  );
  $form['account'] = array(
    '#type' => 'markup',
    '#markup' => '<div>'
      . t(
        'You will need a <a href="!url">Mollie account</a> to use Mollie.',
        array('!url' => $url)
      )
      . '</div>',
  );

  // Live API key.
  $url = url(\Drupal\mollie_payment\MolliePaymentInstaller::MOLLIE_API_KEYS_URL);
  $live_api_key = variable_get('mollie_payment_default_api_key_live', '');
  $description = empty($live_api_key)
    ? t(
      'You can <a href="!url">find your API keys in your Mollie dashboard</a>',
      array('!url' => $url)
    )
    : t('Leave empty to keep current live API key.');
  $form['mollie_payment_default_api_key_live'] = array(
    '#type' => 'password',
    '#title' => t('Live API key'),
    '#description' => $description,
  );

  // Test API key.
  $test_api_key = variable_get('mollie_payment_default_api_key_test', '');
  $description = empty($test_api_key)
    ? t(
      'No API key configured. You can <a href="!url">find your API keys in your Mollie dashboard</a>.',
      array('!url' => $url)
    )
    : t('Leave empty to keep current test API key.');
  $form['mollie_payment_default_api_key_test'] = array(
    '#type' => 'password',
    '#title' => t('Test API key'),
    '#description' => $description,
  );

  // Organization access token.
  $url = url(\Drupal\mollie_payment\MolliePaymentInstaller::MOLLIE_TOKENS_URL);
  $access_token = variable_get('mollie_payment_default_access_token', '');
  $description = empty($access_token)
    ? t(
      'No access token configured. You can <a href="!url">find your organization access token in your Mollie dashboard</a>. Add the "onboarding.read" permission when creating the access token.',
      array('!url' => $url)
    )
    : t('Leave empty to keep current organization access token.');
  $form['mollie_payment_default_access_token'] = array(
    '#type' => 'password',
    '#title' => t('Organization access token'),
    '#description' => $description,
  );

  $form = system_settings_form($form);

  // We need some additional logic to handle the password fields.
  array_unshift($form['#submit'], 'mollie_payment_account_configuration_form_submit');

  return $form;
}

/**
 * Form submit handler for mollie_payment_account_configuration_form.
 */
function mollie_payment_account_configuration_form_submit(array &$form, array &$form_state) {
  if ($form_state['values']['mollie_payment_default_api_key_live'] == '') {
    // Leave unchanged.
    unset($form_state['values']['mollie_payment_default_api_key_live']);
  }

  if ($form_state['values']['mollie_payment_default_api_key_test'] == '') {
    // Leave unchanged.
    unset($form_state['values']['mollie_payment_default_api_key_test']);
  }

  if ($form_state['values']['mollie_payment_default_access_token'] == '') {
    // Leave unchanged.
    unset($form_state['values']['mollie_payment_default_access_token']);
  }
}
